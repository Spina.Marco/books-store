package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"database/sql"
	"github.com/subosito/gotenv"
	"books-list/model"
	"books-list/driver"
	"books-list/controller"
)


	var books []model.Book
	var db *sql.DB

	func init() {
		gotenv.Load()
	}

	func logFatal(err error) {
		if err != nil {
			log.Fatal(err)
		}
	}

	func main() {
		db = driver.ConnectDB()
		controller := controller.Controller{}

		router := mux.NewRouter()

		router.HandleFunc("/books", controller.GetBooks(db)).Methods("GET")
		router.HandleFunc("/books/{id}", controller.GetBook(db)).Methods("GET")
		router.HandleFunc("/books", controller.AddBook(db)).Methods("POST")
		router.HandleFunc("/books", controller.UpdateBook(db)).Methods("PUT")
		router.HandleFunc("/books/{id}", controller.RemoveBook(db)).Methods("DELETE")

		fmt.Println("Server has started on port 8000")

		log.Fatal(http.ListenAndServe(":8000", router))
	}


